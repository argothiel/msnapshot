#!/bin/bash

TIMESTAMP=$(date "+%Y%m%d%H%M")
rsync -v -a --delete --mkpath --relative /etc /root/backup/trzmiel-backup/snapshot_$TIMESTAMP
rsync -v -a --delete --mkpath --relative /home/argothiel /root/backup/trzmiel-backup/snapshot_$TIMESTAMP
