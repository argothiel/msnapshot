#!/bin/bash
mkdir -p /etc
mkdir -p /home/argothiel

rm -f /etc/file1 /etc/.file2 /home/argothiel/file3 /home/argothiel/.file4

echo "Hello file1" > /etc/file1
echo "Hello file2" > /etc/.file2
echo "Hello file3" > /home/argothiel/file3
echo "Hello file4" > /home/argothiel/.file4

faketime '2020-12-31' /doBackup.sh

(cd /etc && find -mindepth 1 -exec stat -c '%A %F %g %u %s %Y %n' {} \;) | sort > /etc.stat
(cd /root/backup/trzmiel-backup/snapshot_202012310000/etc && find -mindepth 1 -exec stat -c '%A %F %g %u %s %Y %n' {} \;) | sort > /backup.etc.stat
(cd /home/argothiel && find -mindepth 1 -exec stat -c '%A %F %g %u %s %Y %n' {} \;) | sort > /argothiel.stat
(cd /root/backup/trzmiel-backup/snapshot_202012310000/home/argothiel && find -mindepth 1 -exec stat -c '%A %F %g %u %s %Y %n' {} \;) | sort > /backup.argothiel.stat

diff /etc.stat /backup.etc.stat && diff /argothiel.stat /backup.argothiel.stat
